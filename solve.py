
def ft_sqrt(val):
	oldguess = 0
	guess = 1
	while abs(guess - oldguess) > 0:
		oldguess = guess
		guess = (guess + val/guess) / 2.0
	return guess



def solveseconddegree(tab):
    a = tab['2']
    b = tab['1']
    c = tab['0']

    print (a, b, c)
    discriminant = b * b - 4 * a * c

    print ("\033[0;32m" + "Discriminant Value : " + "\033[0m" + str(discriminant))
    if (discriminant > 0):
        print("\033[0;32m" + "The discriminant is greater than 0, therefore there are 2 real roots" + "\033[0m")
        x1 = (- b + ft_sqrt(b * b - 4 * a * c)) / (2 * a)
        x2 = (- b - ft_sqrt(b * b - 4 * a * c)) / (2 * a)
        print("\033[0;32m" + "Solutions are :" + "\033[0m" +" x1 = " + str(x1) + " x2 = " + str(x2))
    elif (discriminant == 0):
        print("\033[0;32m" + "The discriminant is equal 0, therefore there is 1 real roots" + "\033[0m")
        print ("x = ", str(- b / 2 * a))
    elif (discriminant < 0):
        print(("\033[0;32m" + "The discriminant is lower than 0, therefore there are no real roots" + "\033[0m"))
        a = 2 * a
        b = - b / a
        x_complex = ft_sqrt(abs(b * b - 4 * a * c)) / a
        print("\033[0;32m" + "Solutions are :" + "\033[0m" + "\nx1 = " + str(b) + "+" + str(x_complex) + "i \nx2 = " + str(b)+ "-" + str(x_complex) + "i")


def solvefirstdegree(tab):
	b = tab['1']
	c = tab['0']

	solution = -c / b
	print("\033[0;32m" + "Solutions is :" + "\033[0m" + str(solution))
