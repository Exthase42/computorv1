from solve import *


def print_reduced_form(tab):

    sorted_key = sorted(tab.keys())
    sorted_key_reverse = sorted(tab.keys(), reverse=True)
    i = 0

    print(tab)

    check = 0
    for key in tab:
        if (tab[key] != 0):
            check = 1;
    if (check == 0):
        print("\033[0;32m" + "The solutions of this equation are all Real numbers" + "\033[0m")
        exit(0)

    elif(tab['0'] != 0 and tab['1'] == 0 and tab['2'] == 0):
        print("\033[0;32m" + "This equation has no solution" + "\033[0m")
        exit(0)


    print ("\033[0;32m" + "Reduced Form = " + "\033[0m", end="")
    if ((tab['2'] == 0) and len(tab) <= 3):
        for key in sorted_key:
            if (i != 2):
                if (tab[key] >= 0 and i >= 1):
                    print ("+ " + str(abs(tab[key])) + " * X^" + key, end=" ")
                elif(i >= 1 and tab[key] < 0):
                    print ("- " + str(abs(tab[key])) + " * X^" + key, end=" ")
                else:
                    print (str(abs(tab[key])) + " * X^" + key, end=" ")
            i += 1
        print ("= 0")
        #print ("\033[0;32m" + "Polynomial Degree : " + "\033[0m" + "1")
    else:
        for key in sorted_key:
            if (tab[key] >= 0 and i >=1):
                print ("+ " + str(abs(tab[key])) + " * X^" + key, end=" ")
            elif(i >= 1 and tab[key] < 0):
                print ("- " + str(abs(tab[key])) + " * X^" + key, end=" ")
            else:
                print (str(abs(tab[key])) + " * X^" + key, end=" ")
            i += 1
        print ("= 0")
        print ("\033[0;32m" + "Polynomial Degree : " + "\033[0m" + sorted_key_reverse[0])

    if (int(sorted_key_reverse[0]) > 2):
        print ("The polynomial degree is stricly greater than 2, I can't solve it for now.")
        exit(0)
    elif(tab['2'] == 0):
        solvefirstdegree(tab)
    else:
         solveseconddegree(tab)
