def print_parse_error(idx, string, message):
    print (string[0:idx] + "\033[0;31m" + string[idx] + "\033[0m" +
    string[idx + 1:len(string)] + " : " + "\033[0;31m" + message + "\033[0m")

def check_for_sign(char):
    if (char == "+" or char == "-" or char == "=" or char == "."):
        return True
    return False

def check_after_sign(idx, string):
    if (string[idx].isdigit() or string[idx] == "x"):
        return True
    if string[idx - 1] == '=' and string[idx] == '-':
        return True
    return False

def check_after_digit(idx, string):
    if (string[idx].isdigit() or string[idx] == "*" or
    check_for_sign(string[idx]) == True):
        return True
    return False

def check_after_x(idx, string):
    if (string[idx] == "^" or check_for_sign(string[idx]) == True):
        return True
    return False

def check_after_special(idx, string):
    if (string[idx].isdigit()):
        return True
    return False

def parse_string(string):
    error = 0
    for idx, char in enumerate(string):
        if (idx + 1 == len(string)):
            break
        elif (char.isdigit()):
            if (check_after_digit(idx + 1, string) == False):
                print_parse_error(idx + 1, string, "Wrong Character after Digit")
                error = 1
        elif char == "+" or char == "-" or char == "=":
            if (check_after_sign(idx + 1, string) == False):
                print_parse_error(idx + 1, string, "Wrong Character after sign")
                error = 1
        elif char == "x":
            if (check_after_x(idx + 1, string) == False):
                print_parse_error(idx + 1, string, "Wrong Character after x")
                error = 1
        elif char == "^":
            if (check_after_special(idx + 1, string) == False):
                print_parse_error(idx + 1, string, "Wrong Character after ^")
                error = 1
    if error == 1:
        exit(-1)
