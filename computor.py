

# 6 * X^1 + 5 * X^2 = X^1 + X^2
# 6 * X + 5 * X^2 = X + X^2

import sys
from parse import *
from reduced import *



def error_message(error_string):
	print (error_string)
	exit(-1)

def getpoly(subString):
	index = 0;
	for idx, char in enumerate(subString):
		if char == '^':
			break
	return subString[idx + 1: len(subString)]

def fillDict(sign, subString):
	error_check = 0;
	for idx, char in enumerate(subString):
		if char == 'x' or char == '*' or char == '=':
			break

	result = subString[0:idx]
	for char in subString:
		if char == ".":
			error_check += 1
		if error_check > 1:
			error_message("Multiple '.' in number. Your Program will now exit")
	if "x" not in subString:
		result = subString
	if "." in subString:
		return sign * float(result)
	else:
		return sign * int(result)


def test_negative_poly(index, s):
	while index < len(s):
		if s[index] == "^":
			# print("midFalse")
			return False
		elif (s[index] == "+" or s[index] == "-" or s[index] == "="):
			# print ("Ici True")
			return True
		index+=1
	if index == len(s):
		return True
	return False


def cutString(idx, s, sign, new_dict):
	index = idx
	while index < len(s):
		if ((s[index] == "=" or s[index] == "+" or s[index] == "-") and index != idx):
			memo = index;
			break
		index+=1
	if index == len(s):
		memo = index

	subString = s[idx:memo]
	# print ("\033[0;31m" + subString + "\033[0m")
	if "x^1" in subString or ("x" in subString and "^" not in subString):
		new_dict['1'] = new_dict['1'] + fillDict(sign, subString)
	elif "x^2" in subString:
		new_dict['2'] = new_dict['2'] + fillDict(sign, subString)
	elif "x^0" in subString:
		new_dict['0'] = new_dict['0'] + fillDict(sign, subString)
	elif "x^" in subString:
		poly = getpoly(subString)
		if poly not in new_dict.keys():
			new_dict[poly] = 0
		new_dict[poly] = new_dict[poly] + fillDict(sign, subString)
	else:
		new_dict['0'] = new_dict['0'] + fillDict(sign, subString)

	return new_dict;



def replace_str_index(text,index=0,replacement=''):
    return '%s%s%s'%(text[:index],replacement,text[index+1:])

def clean_string(s):
	index = -1
	while True:
		index = s.find("x^", index+1)
		if (index == 0 or (index > -1 and (s[index - 1] == "+"
		or s[index - 1] == "-" or s[index - 1] == "="))):
			 s = replace_str_index(s, index, "1*x")
		if index == -1:
			break
	return s


poly = sys.argv[1]
new_dict = {}
new_dict['0'] = 0
new_dict['1'] = 0
new_dict['2'] = 0

original_form = poly
poly = poly.replace(" ", "")
poly = poly.lower()
parse_string(poly)
poly = clean_string(poly)
# print (poly)

if "=" not in poly:
 	error_message("\033[0;31m" +
	"Error : No '=' in expression, your program will now exit" + "\033[0;31m")
if "--" in poly or "++" in poly or ".." in poly:
	error_message("\033[0;31m" +
	"Error :Your expression is not well prototyped, Your program will now exit" + "\033[0;31m")

sign = 1
for idx, char in enumerate(poly):
	if (char == "="):
		if (poly[idx + 1] == '0' and len(poly) == idx + 2):
			break
		sign = - 1
	if char.isdigit() and ((poly[idx - 1] != "^" and poly[idx - 1] != '.'
	and poly[idx - 1].isdigit() == False) or idx == 0):
		if poly[idx - 1] == "+" or poly[idx - 1] == "-":
			new_dict = cutString(idx - 1, poly, sign, new_dict)
		else:
			new_dict = cutString(idx, poly, sign, new_dict)
print(new_dict)
print_reduced_form(new_dict)
